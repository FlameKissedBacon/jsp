package CircuitModel;

import java.text.DecimalFormat;

public class Circuit {
	/**
	 * The logic class containing all the. assessors and mutators
	 * 
	 * @author cberkstresser
	 **/

	DecimalFormat df = new DecimalFormat("#.00");
	//String oneDecimal = df.format();
	/**
	 * stores the voltage.
	 */
	private double pVoltage = 0.0;
	/**
	 * stores the amperage.
	 */
	private double pAmperage = 0.0;
	/**
	 * stores the resistance.
	 */
	private double pResistance = 0.0;

	// return the voltage
	public final double getVoltage() {
		
		return pVoltage;
	}

	// return the amperage
	public final double getAmperage() {
		
		return pAmperage;
	}

	// return the resistance
	public final double getResistance() {
		df.format(pResistance);
		return pResistance;
	}

	// save the voltage
	public final void setVoltage(final double voltage) {
		df.format(pVoltage);
		pVoltage = voltage;
	}

	// save the amperage
	public final void setAmperage(final double amperage) {
		df.format(pAmperage);
		pAmperage = amperage;
	}

	// save the resistance
	public final void setResistance(final double resistance) {
		df.format(pResistance);
		pResistance = resistance;

	}

	// use this to calculate the voltage
	public final void calculateVoltage() {
		pVoltage = pAmperage * pResistance;
	}

	// use this to calculate the amperage
	public final void calculateAmperage() {
		pAmperage = (pVoltage / pResistance);
	}

	// use this to calculate the resistance
	public final void calculateResistance() {
		pResistance = (pVoltage / pAmperage);
	}

	// use this to clear all the variables
	/**
	 * used to clear the values of the three variables.
	 */
	public final void clearValues() {
		pResistance = 0.0;
		pAmperage = 0.0;
		pVoltage = 0.0;
	}
}
