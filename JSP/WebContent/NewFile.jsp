<!DOCTYPE html>

<%@page import="java.time.LocalDate"%>
<%@ page import="CircuitModel.Circuit" %>
<% String s = "";%>
<html>
<head>
<title>Ohms Law Calculator</title>
</head>

<style>
body {
	background-image: url("https://i.imgur.com/3ZQZxcG.jpg");
	background-size: 110%;
	
}
</style>

<body style="background-color: powderblue;">

	<div align="center">
		<h1>
			<font insert color="white">Ohms Law Calculator! </font>
		</h1>
		<br>
		<h2>
			<font insert color="white"> Time to calculate your favourite
				values! Ohms Law! </font> <br> <font insert color="white">
				Enter the Values that you know and click the one you want to
				calculate! </font>
		</h2>
		<h3>
			<font insert color="white">Today's Date is: <%=new java.util.Date()%>
			</font>
		</h3>
		<br> 
	</div>

	<form method="post">
		<div align="center">
			<table style="width: 25%">
				<tr>
					<td><font insert color="white"> Voltage: </font></td>
					<td><input type="number" min="0.01" step=".1" placeholder="Enter Voltage"
						name="txtVoltage" value="<%
						if (request.getParameterMap().containsKey("txtVoltage")
							&& !request.getParameter("txtVoltage").equals("")) {
							s = request.getParameter("txtVoltage");
						}
						if (request.getParameter("CalculateVoltage") !=null 
						&& !request.getParameter("txtResistance").equals("") 
						&& !request.getParameter("txtAmperage").equals("") 
						) {
							Circuit myCircuit = new Circuit();
							myCircuit.setResistance(Double.parseDouble(request.getParameter("txtResistance")));
							myCircuit.setAmperage(Double.parseDouble(request.getParameter("txtAmperage")));
							myCircuit.calculateVoltage();
							s=String.valueOf(myCircuit.getVoltage());
						}
						out.print(s);
						%>"></td>
					<td>
						<button type="submit" name="CalculateVoltage">Calculate
							Voltage</button>
					</td>
				</tr>

				<tr>
					<td><font insert color="white"> Resistance: </font></td>
					<td><input type="number" min="0.01" step=".1"  placeholder="Enter Resistance"
						name="txtResistance" value="<%
						if (request.getParameterMap().containsKey("txtResistance")
							&& !request.getParameter("txtResistance").equals("")) {
							s = request.getParameter("txtResistance");
						}
						if (request.getParameter("CalculateResistance") !=null 
						&& !request.getParameter("txtVoltage").equals("") 
						&& !request.getParameter("txtAmperage").equals("") 
						) {
							Circuit myCircuit = new Circuit();
							myCircuit.setVoltage(Double.parseDouble(request.getParameter("txtVoltage")));
							myCircuit.setAmperage(Double.parseDouble(request.getParameter("txtAmperage")));
							myCircuit.calculateResistance();
							s=String.valueOf(myCircuit.getResistance());
							
							
						}
						out.print(s);
						%>"></td>
					<td>
						<button type="submit" name="CalculateResistance">
							Calculate Resistance</button>
					</td>

				</tr>

				<tr>
					<td><font insert color="white"> Amperage: </font></td>
					<td><input type="number" min="0.01" step=".1" placeholder="Enter Amperage"
						name="txtAmperage"  value="<%
						if (request.getParameterMap().containsKey("txtAmperage")
							&& !request.getParameter("txtAmperage").equals("")) {
							s = request.getParameter("txtAmperage");
						}
						if (request.getParameter("CalculateAmperage") !=null 
						&& !request.getParameter("txtVoltage").equals("") 
						&& !request.getParameter("txtResistance").equals("") 
						) {
							Circuit myCircuit = new Circuit();
							myCircuit.setVoltage(Double.parseDouble(request.getParameter("txtVoltage")));
							myCircuit.setResistance(Double.parseDouble(request.getParameter("txtResistance")));
							myCircuit.calculateAmperage();
							s=String.valueOf(myCircuit.getAmperage());
							
							
						}
						out.print(s);
						%>"></td>
						
						
						
					<td>
						<button type="submit" name="CalculateAmperage">Calculate
							Amperage</button>
							
						
					</td>
				</tr>
			</table>
		</div>
	</form>
	<div align="center">
	<img 
			src="https://d12m281ylf13f0.cloudfront.net/bbsystem-usrimages/s/shocking1.gif"
			height="300">
	</div>
	
</body>
</html>

